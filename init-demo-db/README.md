# initdb

Small tool to initialize a demo database for testing.

First, download Datomic Pro on-prem, see [these instructions](https://docs.datomic.com/on-prem/getting-started/get-datomic.html) for details.

In one terminal run the command
```
./start_demodb.sh
```
In another terminal, run from within this directory:
```
cd initdb
clj -M -m initdb.core
```
