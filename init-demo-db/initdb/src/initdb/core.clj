(ns initdb.core
  (:require [datomic.client.api :as d]
            [clojure.edn :as edn]))

;; See this tutorial:
;; https://docs.datomic.com/on-prem/tutorial/assertion.html

(defn make-demo-db []
  (let [config (-> "../../resources/config.edn"
                   slurp
                   edn/read-string
                   :demo-db)
        client (d/client config)
        conn (d/connect client {:db-name (:name config)})]
    {:client client
     :conn conn}))

(defn make-ident [x]
  {:db/ident x})

(def colors [:red :green :blue :yellow])
(def sizes [:small :medium :large :xlarge])
(def types [:shirt :pants :dress :hat])



(def schema-1
  [{:db/ident :inv/sku
    :db/valueType :db.type/string
    :db/unique :db.unique/identity
    :db/cardinality :db.cardinality/one}
   {:db/ident :inv/color
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :inv/size
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :inv/type
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}])

(def sample-data
  (->> (for [color colors
             size sizes
             type types]
         {:inv/color color
          :inv/size size
          :inv/type type})
       (map-indexed
        (fn [idx map]
          (assoc map :inv/sku (str "SKU-" idx))))
       vec))

(defn initialize [conn]
  (d/transact conn {:tx-data (into []
                                   (comp cat (map make-ident))
                                   [sizes types colors])})
  (d/transact conn {:tx-data schema-1})
  (d/transact conn {:tx-data sample-data}))

(defn -main [& args]
  (-> (make-demo-db)
      :conn
      initialize))
