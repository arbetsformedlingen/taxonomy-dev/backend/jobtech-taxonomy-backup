(ns jobtech-taxonomy-backup.file
  (:require [jobtech-taxonomy-backup.datom :as datom]
            [clojure.edn :as edn]
            [clojure.spec.alpha :as spec]
            [taoensso.nippy :as nippy]))

(spec/def :file/format #{:edn :nippy})
(spec/def :file/filename string?)
(spec/def :file/config (spec/keys :req-un [:file/filename :file/format]))

(def config? (partial spec/valid? :file/config))

(defrecord FileStorage [config])

(defn filename [storage]
  (-> storage
      :config
      :filename))

(defn file-format [storage]
  (-> storage
      :config
      :format))

(defn get-datoms-edn [storage]
  (-> storage
      filename
      slurp
      edn/read-string))

(defn add-datoms-edn [storage datoms]
  ;; See https://nitor.com/en/articles/pitfalls-and-bumps-clojures-extensible-data-notation-edn
  ;; for explanation why we need this.
  (binding [*print-length* nil
            *print-level* nil
            *print-meta* false
            *print-dup* false]
    (spit (filename storage)
          (pr-str datoms))))

(defn get-datoms-nippy [storage]
  (nippy/thaw-from-file (filename storage)))

(defn add-datoms-nippy [storage datoms]
  (nippy/freeze-to-file (filename storage) datoms))

(extend-protocol datom/DatomStorage
  FileStorage
  (get-datoms [storage]
    ((case (file-format storage)
       :edn get-datoms-edn
       :nippy get-datoms-nippy) storage))
  (add-datoms [storage datoms]
    ((case (file-format storage)
       :edn add-datoms-edn
       :nippy add-datoms-nippy) storage datoms)))


(defn file-storage [config]
  {:pre [(config? config)]}
  (FileStorage. config))



