(ns jobtech-taxonomy-backup.datom-utils
  (:require [jobtech-taxonomy-backup.datom :as datom]
            [jobtech-taxonomy-backup.testdata :as testdata]))

(defn datom-transaction-groups "Return a list of lists where each list has the datoms of a transaction"
  [tuples]
  (partition-by datom/datom-tid tuples))

(defn datoms->entity-attribute-map "Given a list of datoms, create a map of all entities and their attributes after all list-forms have been applied."
  [datoms]
  (reduce
   (fn [dst [eid attr value tx add?]]
     (if add?
       (assoc-in dst [eid attr] value)
       (update dst eid dissoc attr)))
   {}
   datoms))

(defn get-ref-attrib-set "Get the set of attributes whose values are refs to other entities"
  [entity-attribute-map]
  (into #{:db.install/attribute}
        (comp (map (fn [[k v]] (if (= :db.type/ref (:db/valueType v)) (:db/ident v))))
              (filter some?))
        entity-attribute-map))


(defn datoms->entity-transaction-map
  "Build a map from every entity id to the first transaction id where it occurs"
  [datoms]
  (reduce (fn [dst [e a v t a?]] (update dst e #(or % t)))
          {}
          datoms))

(defn analyze-entity-refs
  "Detect problems in a sequence of datoms"
  [data]
  (let [groups (datom-transaction-groups data)
        m (datoms->entity-attribute-map data)
        ref-attribs (get-ref-attrib-set m)
        eid-tid-map (datoms->entity-transaction-map data)
        transaction-index-map (into {}
                                    (map-indexed (fn [i group]
                                                   [(-> group
                                                        first
                                                        datom/datom-tid) i]))
                                    groups)]
    (doseq [[e a v t a? :as datom] data]
      (when (ref-attribs a)
        (let [vtrans (eid-tid-map v)
              vtrans-index (transaction-index-map vtrans)
              ttrans-index (transaction-index-map t)]
          (if (or (nil? vtrans-index)
                  (nil? ttrans-index)
                  (< ttrans-index vtrans-index))
            (println "Problematic datom" datom
                     "vtrans-index =" vtrans-index "ttrans-index =" ttrans-index " vtrans =" vtrans)))))
    (println "There are " (count groups) "transactions")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Datoms similarity
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- inc-nil [x]
  (inc (or x 0)))

(defn- build-eid-features
  "Constructs a map from each entity id to a feature value that is invariant of entity id assignment"
  [ref-attrib datoms]
  (let []
    (reduce
     (fn [dst [tid transaction-group]]
       (reduce
        (fn [dst [eid attr value _ add?]]
          (let [step (fn [dst id what & data] (update-in dst [id [tid add? attr what data]] inc-nil))]
            (if (ref-attrib attr)
              (if (= eid value)
                (step dst eid :both)
                (-> dst
                    (step eid :eid)
                    (step value :value)))
              (step dst eid :eid value))))
        dst
        transaction-group))
     {}
     (map-indexed vector (datom-transaction-groups datoms)))))

(defn- datoms-feature
  "Compute a 'feature' value of all the datoms that is invariant of the entity id mapping"
  [datoms]
  (let [ref-attrib (-> datoms
                       datoms->entity-attribute-map
                       get-ref-attrib-set)
        eid-map (build-eid-features ref-attrib datoms)]
    (reduce
     (fn [dst [eid attr value tid add?]]
       (let [key [(eid-map eid) attr (if (ref-attrib attr) (eid-map value) value) (eid-map tid) add?]]
         (update dst key inc-nil)))
     {}
     datoms)))

(defn similar-datoms?
  "Returns true if two sequences of datoms represent the same database. It generally returns false if they don't represent the same database, but can in rare cases return true. This function can nevertheless be used for sanity checking in order to detect common errors."
  [datoms-a datoms-b]
  (= (datoms-feature datoms-a)
     (datoms-feature datoms-b)))
