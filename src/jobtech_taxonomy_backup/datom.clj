(ns jobtech-taxonomy-backup.datom
  (:require [clojure.spec.alpha :as spec]))

(spec/def :datom/eid number?)
(spec/def :datom/attribute keyword?)
(spec/def :datom/value any?)
(spec/def :datom/transaction-id :datom/eid)
(spec/def :datom/add? boolean?)

(spec/def :datom/tuple (spec/cat :eid :datom/eid
                                 :attribute :datom/attribute
                                 :value :datom/value
                                 :transaction-id :datom/transaction-id
                                 :add? :datom/add?))

(def datom-tuples? (partial spec/valid? :datom/tuples))

(defn validate-datom-tuples
  "Check that the datoms are valid according to the spec and return them. If they are not valid, produce an exception with an informative message."
  [tuples]
  (if (datom-tuples? tuples)
    tuples
    (throw (ex-info (str "Invalid datom tuples: " (spec/explain-str :datom/tuples tuples))
                    {:tuples tuples}))))

;; A DatomStorage is a place where datoms can be stored:
;;  * A file in some format
;;  * A datomic database
;;  * A datahike database
;;  * ... something else depending on what we need.
;;
;; The datoms are valid :datom/tuples
(defprotocol DatomStorage
  (add-datoms [this datoms])
  (get-datoms [this]))


;;;; Transactions

(defn datom-eid [[eid _ _ _ _]] eid)
(defn datom-attribute [[_ a _ _ _]] a)
(defn datom-value [[_ _ v _ _]] v)
(defn datom-tid [[_ _ _ tid _]] tid)
(defn datom-add? [[_ _ _ _ add?]] add?)

(spec/def :datom/tuples (spec/coll-of :datom/tuple))
