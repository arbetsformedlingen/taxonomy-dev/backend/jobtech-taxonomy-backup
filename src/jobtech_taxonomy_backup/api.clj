(ns jobtech-taxonomy-backup.api
  (:require [jobtech-taxonomy-backup.core :as core]))

(defn copy
  "Copy all the datoms from :src db to :dst db

  - :config - a filename of a config edn file
  - :check - whether to perform a sanity check after copy
  - :src and :dst are keyword keys in the config file map"
  [{:keys [src dst config check]
    :or {config "resources/config.edn"
         check true}}]
  {:pre [src dst config check]}
  (core/perform-copy
    {:config-map (core/load-config config)
     :check? check
     :src src
     :dst dst}))
