(ns jobtech-taxonomy-backup.core
  (:require [clojure.spec.alpha :as spec]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [jobtech-taxonomy-backup.datom :as datom]
            [jobtech-taxonomy-backup.datom-utils :as datom-utils]
            [jobtech-taxonomy-backup.datomic :as datomic]
            [jobtech-taxonomy-backup.file :as file]))

(spec/def :backup/type keyword?)
(spec/def :backup/config-key keyword?)
(spec/def :backup/storage-config (spec/keys :req-un [:backup/type]))
(spec/def :backup/config-map (spec/map-of :backup/config-key
                                          :backup/storage-config))

(defn load-config
  "Load a configuration"
  [filename]
  (-> filename
      slurp
      edn/read-string))

(def config-map? (partial spec/valid? :backup/config-map))

;; TODO: Consider turn into multimethod if need be.
(defn storage "Construct a DatomStorage from a config by dispatching on :type"
  [src-config]
  ((case (:type src-config)
      :datomic datomic/datomic-storage
      :file file/file-storage) src-config))

(defn copy-datoms!
  "Copy the datoms from one DatomStorage to another DatomStorage and return the copied datoms."
  [src dst]
  (println "   -- Get datoms from storage")
  (let [datoms (-> src
                   datom/get-datoms
                   datom/validate-datom-tuples)]
    (println "   -- Analyze datoms")
    (datom-utils/analyze-entity-refs datoms)
    (println "   -- Add datoms to storage")
    (datom/add-datoms dst datoms)
    (println "   -- Done copying")
    datoms))

(defn storage-at-key [config k]
  (storage (get config k)))

(defn copy-datoms-between-config-keys! [config src dst]
  (let [src (storage-at-key config src)
        dst (storage-at-key config dst)]
    [src dst (copy-datoms! src dst)]))

(defn perform-copy [{:keys [config-map check? src dst]}]
  (println (str "Copy " src " -> " dst))
  (cond
    (not (config-map? config-map))
    (spec/explain :backup/config-map config-map)

    (not (contains? config-map src))
    (println "Missing config for key" src)

    (not (contains? config-map dst))
    (println "Missing config for key" dst)

    :else
    (let [[src dst datoms] (copy-datoms-between-config-keys! config-map src dst)]
      (println "  Copied.")
      (when check?
        (println "  check:"
                 (if (datom-utils/similar-datoms? datoms (datom/get-datoms dst))
                   "Success"
                   "Failure"))))))

(defn demo []
  (let [filename "resources/config.edn"
        config (load-config filename)

        _ (when (not (config-map? config))
            (spec/explain :backup/config-map config)
            (throw (ex-info "Bad config " config)))
        
        demo-db (-> config
                    :demo-db
                    storage)
        backup-file (-> config
                        :backup-file
                        storage)
        backup-file-2 (-> config
                          :backup-file2
                          storage)

        dev-db (-> config
                  :dev-db
                  storage)

        dev-backup-file (-> config
                            :dev-backup-file
                            storage)

        ;;[src dst] [demo-db backup-file]
        ;;[src dst] [backup-file demo-db]
        ;;[src dst] [demo-db backup-file-2]
        ;;[src dst] [dev-db dev-backup-file]
        [src dst] [dev-backup-file demo-db]

        datoms (copy-datoms! src dst)
        ;;datoms (datom/get-datoms src)

        ,]
    (println "Copied" (count datoms) "tuples")
    datoms))

(defn demo-compare []
  (let [filename "resources/config.edn"
        config (load-config filename)

        _ (when (not (config-map? config))
            (spec/explain :backup/config-map config)
            (throw (ex-info "Bad config " config)))
        
        demo-db (-> config
                    :demo-db
                    storage)
        backup-file (-> config
                        :backup-file
                        storage)
        backup-file-2 (-> config
                          :backup-file2
                          storage)

        dev-db (-> config
                  :dev-db
                  storage)

        dev-backup-file (-> config
                            :dev-backup-file
                            storage)

        datoms-a (datom/get-datoms dev-backup-file)
        datoms-b (datom/get-datoms backup-file-2)]
    (println "Datoms-a: " (count datoms-a) (take 4 datoms-a))
    (println "Datoms-b: " (count datoms-b) (take 4 datoms-b))
    (datom-utils/similar-datoms? datoms-a datoms-b)))



;; (demo)

(comment
  (perform-copy
    {:config-map {:src {:type :datomic
                        :name "jobtech-taxonomy-dev-2021-03-05-16-30-40"
                        :read-only? true
                        :server-type :ion
                        :region "eu-central-1"
                        :system "tax-test-v4"
                        :endpoint "http://entry.tax-test-v4.eu-central-1.datomic.net:8182/"
                        :proxy-port 8182}
                  :dst {:type :datomic
                        :name "jobtech-taxonomy-vlaaad-2021-06-15-13-19-02"
                        :server-type :ion
                        :region "eu-central-1"
                        :system "tax-test-v4"
                        :endpoint "http://entry.tax-test-v4.eu-central-1.datomic.net:8182/"
                        :proxy-port 8182}}
     :check? true
     :src :src
     :dst :dst})
  ,)