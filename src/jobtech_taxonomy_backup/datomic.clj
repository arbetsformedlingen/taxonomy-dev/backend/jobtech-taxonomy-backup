(ns jobtech-taxonomy-backup.datomic
  (:require [datomic.client.api :as datomic]
            [jobtech-taxonomy-backup.datom :as datom]
            [wanderung.datomic-cloud :refer [extract-datomic-cloud-data]]
            [jobtech-taxonomy-backup.datom-utils :as datom-utils]))

;; This code is based on
;; https://github.com/lambdaforge/wanderung/blob/development/src/wanderung/datomic_cloud.clj

(defn extract-datoms [conn]
  (extract-datomic-cloud-data conn))

(defn break-out-keys [src & ks]
  (into [(reduce dissoc src ks)]
        (map src)
        ks))

(defrecord DatomicStorage [config name read-only?])

(defn temp-id [eid]
  (str "tmp-" eid))

(defn map-value-ref
  "Maps the value part of a datom to either a temp id or a true entity id"
  [context value]
  (let [{:keys [tempids datom eid-tid-map]} context]
    (if (keyword? value)
      value
      (or (get tempids (temp-id value))
          (let [this-tid (datom/datom-tid datom)
                value-tid (get eid-tid-map value)]
            (assert (number? this-tid))
            (assert (number? value-tid))
            (if (= this-tid value-tid)
              (temp-id value) ;; <-- The entity is introduced in this transaction.
              (throw (ex-info "Failed to map value ref"
                              {:value value
                               :tempids tempids
                               :datom datom
                               :referred-entity (get (:entity-map context) value)}))))))))

(def transaction-temp-id "datomic.tx")

(defn datom->list-form
  "Transforms a datom to list-form to be part of a transaction. Entity ids will be mapped."
  [context [eid attrib value tid add? :as datom]]
  (let [tempids (:tempids context)
        context (assoc context :datom datom)
        ref-attribs (:ref-attribs context)
        _ (assert ref-attribs)
        temp-eid (temp-id eid)]
    [ ;; Whether to add or retract
     (if add? :db/add :db/retract)

     ;; The entity id: either it is a temp id for a new entity,
     ;; or a true entity id. In the special case that the entity id is *this* transaction,
     ;; the special tempid "datomic.tx" is used.
     (if (= eid tid) ;; 
       transaction-temp-id
       (get tempids temp-eid temp-eid))

     ;; The attribute
     attrib

     ;; The value: If it is a reference to another entity,
     ;; the correct entity id needs to be figured out.
     (if (ref-attribs attrib)
       (if (= value tid)
         transaction-temp-id
         (map-value-ref context value))
       value)]))

(defn tx-data-from-datoms [context datoms]
  "Map datoms to list-forms to be the tx-data of a transaction"
  (into []
        (comp (remove #(#{:db.install/attribute} (datom/datom-attribute %)))
              (map (partial datom->list-form context)))
        datoms))

(defn perform-transaction
  "Perform a transaction with the datoms and return an updated context"
  [conn context datoms]
  (let [tid (-> datoms first datom/datom-tid)
        tx-data (tx-data-from-datoms context datoms)
        _ (when (:verbose? context)
            (println "Perform transaction of" (count tx-data) "datoms, tid =" tid))
        result (datomic/transact
                conn
                {:tx-data tx-data})
        new-tempids (:tempids result)]
    (update context
            :tempids
            #(-> %
                 (merge new-tempids)
                 (assoc (temp-id tid) (get new-tempids transaction-temp-id))))))


(defn datomic-storage "Construct a DatomicStorage from a configuration"
  [config]
  (let [[config datomic-name datomic-type read-only?] (break-out-keys config :name :type :read-only?)]
    (DatomicStorage. config datomic-name read-only?)))

(defn connect "Get a datomic connection from a DatomicStorage"
  [^DatomicStorage storage]
  (datomic/connect (datomic/client (:config storage))
                   {:db-name (:name storage)}))

(defn add-datoms-to-datomic [storage datoms]
  (let [conn (connect storage)
        entity-map (datom-utils/datoms->entity-attribute-map datoms)
        ref-attribs (datom-utils/get-ref-attrib-set entity-map)
        transaction-groups (datom-utils/datom-transaction-groups datoms)]
    (reduce (partial perform-transaction conn)
            {:ref-attribs ref-attribs
             :entity-map entity-map
             :eid-tid-map (datom-utils/datoms->entity-transaction-map datoms)
             :verbose? false
             :datom nil
             :tempids {}}
            transaction-groups)))

(extend-protocol datom/DatomStorage
  DatomicStorage
  (add-datoms [storage datoms]
    (when (:read-only? storage)
      (throw (ex-info "Cannot add datoms: datomic storage is read-only" {:storage storage})))
    (add-datoms-to-datomic storage datoms))
  (get-datoms [storage]
    (-> storage
        connect
        extract-datoms)))

;; Schema modeling:

;; https://docs.datomic.com/cloud/schema/schema-modeling.html
