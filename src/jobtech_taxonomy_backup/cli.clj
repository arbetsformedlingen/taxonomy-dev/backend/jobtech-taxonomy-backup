(ns jobtech-taxonomy-backup.cli
  (:require [jobtech-taxonomy-backup.core :as backup]
            [jobtech-taxonomy-backup.datom-utils :as datom-utils]
            [jobtech-taxonomy-backup.datom :as datom]
            [clojure.spec.alpha :as spec]))

(spec/def ::args (spec/* (spec/alt :help #{"-h" "--help"}
                                   :verbose #{"-v" "--verbose"}
                                   :config (spec/cat :prefix #{"--config" "-c"}
                                                     :filename string?)
                                   :check #{"--no-check"}
                                   :list #{"--list"}
                                   :copy (spec/cat :prefix #{"copy"}
                                                   :src string?
                                                   :dst string?))))

(def usage "USAGE:

  -c, --config [FILENAME] : Load a config file

  -h, --help              : Display help message

  copy [SRC] [DST]        : Copy from database with key SRC to database with key DST

  --no-check              : Disable sanity check after copy

  --list                  : List available storages

Example usage:

  clj -M:cli --verbose --config resources/config.edn copy dev-db dev-backup

This will copy all the datoms from :dev-db to :dev-backup.

")

(defn process-args [args]
  (reduce (fn [options [arg-type arg-data]]
            (case arg-type
              :help (assoc options :help? true)
              :verbose (assoc options :verbose? true)
              :config (update options :config-map merge (-> arg-data :filename backup/load-config))
              :copy (update options :copy conj arg-data)
              :no-check (assoc options :check? false)
              :list (assoc options :list? true)))
          {:copy []
           :help? false
           :config-map {}
           :verbose? false
           :check? true
           :list? false}
          args))


(defn -main [& args0]
  (let [args (spec/conform ::args args0)]
    (if (= ::spec/invalid args)
      (println (str "Invalid input\n\n" usage))
      (let [options (process-args args)
            {:keys [list? verbose? help? copy config-map]} options]
        (when list?
          (println "Storages:" (mapv name (keys config-map))))
        (when help?
          (println usage))
        (when verbose?
          (println "Options: ")
          (clojure.pprint/pprint options))
        (doseq [cp (:copy options)]
          (backup/perform-copy (assoc options
                                 :src (-> cp :src keyword)
                                 :dst (-> cp :dst keyword))))))))
