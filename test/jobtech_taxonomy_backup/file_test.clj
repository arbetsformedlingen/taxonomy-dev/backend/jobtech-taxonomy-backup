(ns jobtech-taxonomy-backup.file-test
  (:require [clojure.test :refer :all]
            [jobtech-taxonomy-backup.file :as file]
            [jobtech-taxonomy-backup.datom :as datom]
            [jobtech-taxonomy-backup.testdata :as testdata])
  (:import [java.io File]))

(defn test-add-and-get [config]
  (let [storage (file/file-storage config)]
    (is (= testdata/value
           (do (datom/add-datoms storage testdata/value)
               (datom/get-datoms storage))))))

(deftest save-test-edn
  (test-add-and-get {:type :file
                     :format :edn
                     :filename (str (File/createTempFile "ednfile" ".edn"))})
  (test-add-and-get {:type :file
                     :format :nippy
                     :filename (str (File/createTempFile "nippyfile" ".nippy"))}))

;;File.createTempFile("prefix",null,myDir);
