(ns jobtech-taxonomy-backup.datom-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [jobtech-taxonomy-backup.datom :as datom]
            [jobtech-taxonomy-backup.datom-utils :as datom-utils]))

(def testdata jobtech-taxonomy-backup.testdata/value)

(deftest validation-test
  (is (spec/valid? :datom/tuples testdata)))

(deftest ea-map-test
  (let [m (datom-utils/datoms->entity-attribute-map testdata)]
    (-> m
        (get-in [17592186045426 :db/ident])
        (= :green)
        is)
    (is (= #{:db.install/attribute :inv/color :inv/type :inv/size}
           (datom-utils/get-ref-attrib-set m)))))

(defn parameterized-datoms [eid-a eid-b tid]
  [[eid-a :person/name "Rudolf" tid true]
   [eid-b :person/name "Vera" tid true]])

(deftest normalized-equal-test
  (is (datom-utils/similar-datoms? (parameterized-datoms 3 5 9)
                                   (parameterized-datoms 4 9 20)))
  (is (not (datom-utils/similar-datoms? (parameterized-datoms 3 5 9)
                                        (assoc-in (parameterized-datoms 4 9 20)
                                                  [1 2] 119)))))
