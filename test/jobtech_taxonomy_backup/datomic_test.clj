(ns jobtech-taxonomy-backup.datomic-test
  (:require [clojure.test :refer :all]
            [jobtech-taxonomy-backup.datomic :as datomic]))

(def sample-datom [119 :person/name "Mjao" nil true])
(def sample-datom-2 [119 :person/mother 120 nil true])

(deftest transaction-state-test
  (is (= [:db/add "tmp-119" :person/name "Mjao"]
         (datomic/datom->list-form {:ref-attribs #{} :tempids {}}
                                   sample-datom)))
  (is (= [:db/add 49 :person/name "Mjao"]
         (datomic/datom->list-form {:ref-attribs #{}
                                    :tempids {"tmp-119" 49
                                              "tmp-120" 50}}
                                   sample-datom)))
  (is (= [:db/add 17 :person/mother 1024]
         (datomic/datom->list-form {:ref-attribs #{:person/mother}
                                    :tempids {"tmp-119" 17
                                              "tmp-120" 1024}}
                                   sample-datom-2))))
