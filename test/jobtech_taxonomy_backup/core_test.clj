(ns jobtech-taxonomy-backup.core-test
  (:require [clojure.test :refer :all]
            [jobtech-taxonomy-backup.core :refer :all]
            [clojure.spec.alpha :as spec]))

(deftest a-test
  (is (spec/valid? :backup/config-map {:db {:type :datomic}
                                       :backup-file {:type :file}}))
  (is (not (spec/valid? :backup/config-map {:db {:type :datomic}
                                            :backup-file {:type :file}
                                            :kattsk {:a 9}}))))
