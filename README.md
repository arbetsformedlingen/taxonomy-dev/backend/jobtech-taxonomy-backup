# jobtech-taxonomy-backup [OBSOLETE]

The code in this repository is **obsolete** and has been replaced by the code in [jobtech-taxonomy-api-gitops](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops), notably the command `make download-prodwrite-db` which will save the prod write database to a file. This file then has to be copied to the correct directory of [jobtech-taxonomy-api-backup-files](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-backup-files).

Once enough time has passed and we are sure we don't need this repository any longer, we should remove it.

~~This project is used to create and to restore taxonomy backups from a datomic database. The backups are stored as lists of datoms in some format. Currently, edn-format is supported.~~

## Usage

In order to make a backup, the configuration of the backup file and the database needs to be encoded in a configuration file as a map of keys and corresponding configurations.

From the command line interface, the `copy` command copies the datoms from one such configuration referred to by its key to another configuration referred to by its key, e.g.

```
clj -X:copy :config '"resources/config.edn"' :src :dev-db :dst :dev-backup
```
This will load the configuration at `resources/config.edn`. It will then copy the contents from the dev database configured at `:dev-db` to the backup file configured at `:dev-backup`.

This is an example of what the configuration file `resources/config.edn` could be:
```
{:dev-db {:type :datomic
          :name "jobtech-taxonomy-dev-2021-03-05-16-30-40"
          :read-only? true
          
          :server-type :ion
          :region "eu-central-1" ;; e.g. us-east-1
          :system "tax-test-v4"
          ;;:creds-profile "<your_aws_profile_if_not_using_the_default>"
          :endpoint "http://entry.tax-test-v4.eu-central-1.datomic.net:8182/"
          :proxy-port 8182}

 :dev-backup {:type :file
              :format :nippy
              :filename "backups/dev.nippy"}}
```

You can use cli entry point, e.g.
```
clj -M:cli --help
```
which will display
```
USAGE:

  -c, --config [FILENAME] : Load a config file

  -h, --help              : Display help message

  copy [SRC] [DST]        : Copy from database with key SRC to database with key DST

  --no-check              : Disable sanity check after copy

  --list                  : List available storages

Example usage:

  clj -M:cli --verbose --config resources/config.edn copy dev-db dev-backup

This will copy all the datoms from :dev-db to :dev-backup.
```


## Running tests

```
clj -X:test
```

## Scripts

There is a script `prod_backup_all.sh` that performs a full backup of the prod databases.

## License

EPL-2.0
Copyright © 2021 Jobtech
